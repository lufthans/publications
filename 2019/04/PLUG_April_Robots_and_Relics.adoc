== PLUG April Robots and Relics
:author: der.hans - https://www.LuftHans.com/talks/
:copyright: 2019 der.hans --- CC BY-SA 4.0 unported
:website: https://www.LuftHans.com/
:tags: FLOSSevent, PLUG, UserGroup, Phoenix, FreeSoftware, history, robotics, ros
:imagesdir: images

:PLUG: https://phxlinux.org/
:FLOSS_Stammtisch: https://www.LuftHans.com/Free_Software_Stammtisch
:jobs_night: https://www.LuftHans.com/Free_Software_Stammtisch#JobsNights
:PLUG_mtg_page: https://phxlinux.org/index.php/meetings/14-east-valley-meeting.html
:PLUG_mtg_page_gt: https://phxlinux.org/index.php/meetings/14-east-valley-meeting.html
:Bill_title: 75 Years of Computing in 60 Minutes
:SUNI_title: SUNI the Robot presents STEM in America open source addition

image::PLUG-banner2-variation2-25th_anniv-v32.png[PLUG 25th anniversary banner, width=75%]

For {PLUG_mtg_page}[April] PLUG is delving both into history and the future with Robots and Relics.

Bill will cover {Bill_title}. He will be bringing the https://americanhistory.si.edu/collections/search/object/nmah_692464[nanosecond] Grace Hopper gave him and other historical hardware.

Scott and Wendy have {SUNI_title}. They built SUNI the Robot to inspire interest in STEM fields. They will present a view on the future of robotics and cover use of Free Software such as Ubuntu and Robot Operating System (ROS) for SUNI. Scott and Wendy will also demo SUNI.

We are planning a photo shoot with SUNI and Bill's artifacts.

https://gettogether.community/events/990/east-valley-meeting/[RSVP via GetTogether] if you like.

Thursday, April 11th at 19:00

Desert Breeze Substation, 251 North Desert Breeze Blvd, Chandler, AZ 85226

This month we will also a {jobs_night}[job networking event] before the {FLOSS_Stammtisch}[Free Software Stammtisch] on the 16th.
