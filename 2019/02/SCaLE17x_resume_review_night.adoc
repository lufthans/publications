== Jobs BoF resume reviews at SCaLE17x
:author: der.hans - https://www.LuftHans.com/talks/
:copyright: 2019 der.hans --- CC BY-SA 4.0 unported
:date: 2019Feb21
:website: https://www.LuftHans.com/
:tags: SCaLE17x, SCaLE, jobs, resume, BoF, FLOSSconf, FLOSSevents

:ResumeBoFURL: https://www.socallinuxexpo.org/scale/17x/presentations/job-and-career-search-tips-and-best-practices

At this year's Jobs BoF we will be reviewing resumes.
Bring resumes for review by tech industry veterans at the SCaLE 17x jobs BoF.

{ResumeBoFURL}[Friday night at 19:00] (7pm PST) we will have tech engineering managers and recruiters on hand in Ballroom C.
Resume reviews will run for an hour with informal career networking continuing for at least another hour in Ballroom C.

We will also again have the popular SCaLE jobs board posted all weekend by the
entrance between Ballroom B and Ballroom C.
The jobs board is a great place to place ads for openings at your company and to find openings for job-seekers.

If you are an engineering manager or recruiter wanting to volunteer to help with the resume reviews, please contact us.

Email - jobsBoF (_at_) socallinuxexpo.org

Mastodon - https://mastodon.social/@lufthans

IRC - LuftHans or jillr on Freenode, usually in #PLUGaz and #LOPSA

Twitter - @jillrouleau or @iloveGarick

TODO: Bring your resume and questions for review by tech industry veterans.

When: Friday, March 8th @ 19:00

Where: Ballroom C at SCaLE 17x in Pasadena

Hope to see you there!

Your SCaLE jobs BoF team,

Garick, Jill, Matti and hans
