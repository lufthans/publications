== Using uMatrix to replace NoScript
:author: der.hans - https://www.LuftHans.com/talks/
:copyright: 2019 der.hans --- CC BY-SA 4.0 unported
:date: 2019Feb24
:website: https://www.LuftHans.com/
:tags: uMatrix, Firefox, privacy, security, web, Mozilla, JavaScript, cookies, XHR, frames
:imagesdir: images

:FaiF-site: http://www.faif.us/
:FaiF-ep0x61: http://www.faif.us/cast/2019/feb/19/0x61/

:sfconservancy-site: https://sfconservancy.org/

:uMatrix-site: https://addons.mozilla.org/en-US/firefox/addon/umatrix/
:NoScript-site: https://addons.mozilla.org/en-US/firefox/addon/noscript/
:uBlock_Origin-site: https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/
:uBO-Scope-site: https://addons.mozilla.org/en-US/firefox/addon/ubo-scope/

:uMatrix-author: Raymond Hill
:uMatrix-author-site: https://addons.mozilla.org/en-US/firefox/user/11423598/


:wikipedia-site: https://en.wikipedia.org/
:bbc-site: https://www.bbc.co.uk/
:newegg-site: https://www.newegg.com/

In the most recent episode of {FaiF-ep0x61}[Free as in Freedom (ep 0x61)] Bradley mentioned having trouble using {NoScript-site}[NoScript] with current web sites.

As a long-time fan of NoScript, I understand his frustration.
Fortunately, there's now {uMatrix-site}[uMatrix].

uMatrix has similar functionality to NoScript, but with a much better visual interface and more features.
Importantly, uMatrix controls cookies and more in addition to JavaScript.

uMatrix allows setting JavaScript, cookie, CSS, image and media controls per web site.
For each web site, any of the controlled features can be allowed for specific domains, including the origin site.

For instance, the {FaiF-site}[Free as in Freedom] site has 2 CSS files, 5 graphics and 3 JavaScript files coming from itself when allowing only CSS and graphics.

:image-faif-no-1st-party: uMatrix.faif.no-1st-party.b.png

image::{image-faif-no-1st-party}[uMatrix grid for FaiF site denying 1st party cookies and JavaScript]

The JavaScript files might pull in other resources, but I block that.

The hosts of FaiF, Bradley and Karen, both work for the {sfconservancy-site}[Software Freedom Conservancy] providing excellent service for the Free Software community.

'sfconservancy.org' site has calls to 5 CSS files, 3 graphics and 5 JavaScript files from 'sfconservancy.org'.
Again, I override the default and block the JavaScript even from the original domain.  
The site also pulls in 1 cookie from 'creativecommons.org' and 1 graphic from 'i.creativecommons.org'.
My default uMatrix setup blocks the cookie and allows the graphic.
Finally, 'sfconservancy.org' uses 1 cookie and 1 graphic from licensebuttons.net.

image::uMatrix.sfconservancy.no-1st-party.no-cc-cookie.b.png[uMatrix grid for sfconservancy.com denying 1st party cookies and JavaScript]

Again, the cookie is blocked and the graphic allowed.

I can easily use the uMatrix drop down menu to allow the Creative Commons cookie if I need to.
Note it jumps from 1 to 4 cookies after reload.

image::uMatrix.sfconservancy.yes-cc-cookie.b.png[uMatrix grid for sfconservancy.com denying 1st party cookies and JavaScript, but allowing Creative Commons cookies]

That permission for 'creativecommons.org' cookies only applies to cookies requested by loading 'sfconservancy.org' pages.
Any other site, including the Creative Commons' own site, would still not be allowed to set Creative Commons cookies.

image::uMatrix.cc.no-1st-party.b.png[uMatrix grid showing Creative Commons not having cookies]

:Wikipedia-XHR-page: https://en.wikipedia.org/wiki/XMLHttpRequest

uMatrix has default rules that block all JavaScript, {Wikipedia-XHR-page}[XHR] ( background data requests ), Media ( video, flash, sound ), and frames.
The default rules then allow everything from the site being viewed and its subdomains.
The uMatrix rules call the origin site __1st-party__.

For instance, when visiting '{wikipedia-site}[{wikipedia-site}]', the default behavior set rules for 'wikipedia.org' (dark blue in the upper left) and allows all objects from both 'wikipedia.org' and 'en.wikipedia.org'.

image::uMatrix.Wikipedia.default.b.png[uMatrix grid for wikipedia.org rules when viewing en.wikipedia.org site]

uMatrix understands at least some localized naming, e.g. when visiting '{bbc-site}[{bbc-site}]' uMatrix smartly defaults to rules for 'bbc.co.uk' (dark blue in the upper left) rather than for 'co.uk'.

image::uMatrix.bbc-co-uk.default.b.png[uMatrix grid for bbc.co.uk]

{newegg-site}[newegg] is an example site with many 3rd party dependencies.
Look at it with uMatrix installed.
As you allow more JavaScript and reload even more 3rd party dependencies appear.
For instance, allowing the 'googlecommerce.com' JavaScript file and reloading adds a request for a 'www.google.com' JavaScript file.

uMatrix helps you control which sites are allowed to use JavaScript and cookies and when.
I highly recommend installing the uMatrix add on for your browser.
I use it with Firefox.
It is also available for Chrome and Chromium.

My recommendation is to stick with the default rules to begin with.
Once familiar with uMatrix consider removing the `* 1st-party * allow` and `* 1st-party frame allow` rules for additional security and privacy.

Some sites are a pain.
The Daily Show's site requires all kinds of junk to see the videos.
Humble Bundle requires Stripe assets to purchase a bundle and suddenly requires Google assets so you can prove you're not a robot.
The books are DRM free, but the site has issues.

Even if you have to just allow all objects for a few sites, at least most sites aren't getting full JavaScript access to your system.
I personally dedicate a Firefox profile to annoying sites that I want to use despite their 3rd party JavaScript and cookie dependencies.

:lh-firefox-profile-manager-site: https://gitlab.com/lufthans/lh-firefox-profile-manager

I actually use many Firefox profiles with different instances.
For example, I have a Firefox instance dedicated to Mastodon and one for my bank and another for job searches.
The Daily Show and Humble Bundle also have dedicated Firefox profiles.
I'll leave documenting that setup to a different post.
I did post my {lh-firefox-profile-manager-site}[rather simple scripts] for managing Firefox profiles to GitLab.

:SCaLE17x-talk-title: Device and Personal Privacy Technology Roundup
:SCaLE17x-talk-url: https://www.socallinuxexpo.org/scale/17x/presentations/device-and-personal-privacy-technology-roundup

Raymond Hill, creator of uMatrix, also created the very popular {uBlock_Origin-site}[uBlock Origin] add on.
In reviewing his work for this post I see he also created {uBO-Scope-site}[uBO Scope] for measuring 3rd party exposure on web sites.
I need to experiment with that before my SCaLE 17x talk, {SCaLE17x-talk-url}[{SCaLE17x-talk-title}], in March. 

:sfconservancy-donate-page: https://sfconservancy.org/supporter/
:mozilla-donate-page: https://donate.mozilla.org/en-US/?presets=50,30,20,10&amount=42&utm_source=mozilla.org&utm_medium=referral&utm_content=nav&currency=usd

In this post I mention the three tools from Raymond Hill and also Software Freedom Conservancy.
I don't see an option to donate to Raymond Hill, but Software Freedom Conservancy certainly {sfconservancy-donate-page}[accepts donations] to support their important work.
I use Mozilla's Firefox as my browser with uMatrix for this article.
Mozilla will also gladly {mozilla-donate-page}[accept donations].

__A note about NoScript__

I happily used NoScript for many years.
Thanks to Giorgio Maone for creating and maintaining a tool that I depended on.
It was and is still a great tool.
When Firefox released Quantum NoScript did not have a web extension version available.

JavaScript and cookie blocking are important browser features for me.
Lack of NoScript led me to seek an alternative.
When I tried uMatrix, saw the interface and discovered it also controls cookies I was glad I had needed to look around.

For both tools, look at their configuration options for extra features.

Thanks again to the developers of both tools for improving my web browsing experience.

__Further information on XHR.__

:w3schools-XHR-page: https://www.w3schools.com/xml/xml_http.asp

From the {w3schools-XHR-page}[w3schools web site]:
----
The XMLHttpRequest object is a developers dream, because you can:

    Update a web page without reloading the page
    Request data from a server - after the page has loaded
    Receive data from a server  - after the page has loaded
    Send data to a server - in the background
----
