<!--
title: Cooperative Competition, a Free Software Advantage
author: der.hans
copyright: 2018
license: CC BY-SA 4.0 unported
tags: Free Software, Free Enterprise, Cooperative Competition
-->

# Cooperative Competition, a Free Software Advantage

Free Software flourishes via cooperative competition.
Cooperative competition is an enterprise environment where projects compete with each other while simultaneously benefiting from cooperation.

Free Software projects compete for resources in a truly Free Enterprise market.
Licensing and the openess of projects allows them to also cooperate and benefit from each other.
The cooperative competition brings advantage for those using the software.

Competition provides incentive for the Free Software ecosystem in many ways.

Some advantages are customer-centric.
For instance, there's no vendor lock-in since Free Software doesn't hold data hostage in proprietary formats.
This makes it relatively easy to move from one project to another.
Even without changing projects, a customer can change support vendors.

One agent of change is that Free Software doesn't hold your data hostage.
For instance, a Free Software accounting project such as GnuCash doesn't lock you out of your data.
GnuCash supports three different Free Software SQL databases ( MySQL, PostgreSQL and SQLite ).
You can move from MySQL to PostgreSQL or vice versa.
You can also change from Oracle's MySQL to Percona or to MariaDB, both provide MySQL compatible derivative projects.
Changing the database still requires planning and testing, but there are no proprietary binary formats in Free Software.

Some competition is traditional.
Free Software projects and companies compete directly on product quality and vision.
If the software has problems or an inadequate roadmap customers can move to competing software.
The projects also compete on quality of support, including tools and documentation.
Customers win by having Oracle Corporation and Percona LLC competing for their business.

Free Software projects compete for the same scarce resources.
Developers, documenters, funding, and other resources are limited.
Even one-person projects are competing for that person's time and efforts.
Projects need people and companies that will create the project, maintain it and put in time, money and effort.

Cooperation is supported by the licensing and community models.

Free Software projects cooperate whether they want to or not.
Rival projects are free to copy ideas and features.
The Free Software model actually encourages derivative projects.

From a proprietary perspective, this seems bad.
From a Free Software perspective, it's natural and leads to more innovation.
Projects build off each other rather than locking features out of the marketplace.

Due to the nature of Free Software licensing a project can copy not just features, but also fork the entire project to create a derivative.
One of my favorite pieces of software has benefited from both options.
KeePassX copied the features of KeePass, but is an independent project in a different programming language.
Recently KeePassXC was created as a community fork after years of slow updates on KeePassX ( thanks to those who have been working on both projects! ).
KeePassXC has already added some great new features.
If the codebases haven't diverged too much, KeePass can easily import those features if it wants.
As a fan and customer of the software I hope all three groups are getting along and am glad to benefit from the new work.

KeePassX and KeePassXC use KeePass' encrypted file format to store sensitive data like passwords.
Even though KeePass was a Windows application, KeePassX did not need to reverse engineer the encrypted file format when creating an application for Linux.
Since then KeePassX has become cross platform and we also have compatible Free Software applications for smart phones.

Free Software also benefits from third party cooperation.
Likely the most active cooperation for Free Software takes place in the distributions.
Distributions put a lot of effort into verifying various Free Software projects work together.
They verify KeePassX and KeePassXC work with the version of libcrypt in the distribution.
They also work to make sure competitive projects, such as KDE and GNOME desktop environments, don't stomp on each other.
The distributions even work to make it easy to switch from KDE to GNOME or vice versa or even to another desktop environment.

KeePassX and KeePassXC are two of many third party applications using libcrypt.
Each has self-interest in libcrypt being secure and efficient.
If one audits libcrypt and finds a bug the fix will benefit all projects depending on libcrypt.

The Free Software community is built on sharing.
One of the greatest security issues actually demonstrated the advantages of the Free Software model and reminded us to use them.

Heartbleed is a security bug in a commonly used Free Software project.
It allowed data to be stolen from web sites.
Due to Heartbleed many projects and companies worked together to patch the bug and quickly deploy the updates worldwide.
Unfortunately the project, OpenSSL, had gone without much scrutiny for a variety of reasons and the Heartbleed bug went undetected.

As a response to Heartbleed both the community and many corporations that depend on Free Software stepped up reviews of critical software.
Corporations worked together to start a new initiative to support and audit critical Free Software projects such as OpenSSL.
Also, multiple OpenSSL forks were created to better audit and secure the library.
Subsequently many other security issues were resolved.
Both the third party auditing and the new derivative projects demonstrate that the cooperative competition advantages are passed on to those using the software.
Even if OpenSSL had ignored both, improvements can be made and projects can move off OpenSSL.

In conclusion, cooperative competition is an advantageous process for Free Software.
Free Software benefits from competitive drive in many ways.
It also benefits from several forms of cooperation.

This dual advantage improves software and encourages a truly competitive marketplace.
In that marketplace people and organizations using the software benefit.
They can switch to competitive support or projects without the added cost of escaping vendor lock-in.
The software they're using can also get improvements via several forms of project collaboration.

&copy; 2018 der.hans --- CC BY-SA 4.0 unported
