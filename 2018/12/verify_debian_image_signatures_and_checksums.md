<!--
   :author: der.hans
   :copyright: 2016, 2018 der.hans --- CC BY-SA 4.0 unported
   :license: CC BY-SA 4.0 unported
   :tags: debian, ISO, checksum, security
-->


## Verifying debian ISO signatures

[Debian's CD verification page](https://www.debian.org/CD/verify) doesn't explain how to find and use the keyring from the package.

This page will explain how to get the keyring package and use it to verify the gpg signatures for debian's checksum files. The directions will likely work for other debian-based distributions.

Install debian-keyring ( `sudo apt install debian-keyring` ). Also install gnupg if it isn't already installed ( see [GnuPG sidebar](#GnuPG_sidebar) for details ).

Debian packages install keyrings in `/usr/share/keyrings/`. The `debian-role-keys.gpg` file has the CD image signing keys.

Note: rather than having dozens of files named SHA512SUMS, I rename them to match the source. For the examples here **debian-8.5.0-openstack** was prepended as the files used in this document are from <http://cdimage.debian.org/cdimage/openstack/8.5.0/>.

```
$ gpg2 --keyring /usr/share/keyrings/debian-role-keys.gpg --verify debian-8.5.0-openstack.SHA512SUMS.sign
gpg: assuming signed data in 'debian-8.5.0-openstack.SHA512SUMS'
gpg: Signature made Thu Jun  9 19:39:03 2016 MST using RSA key ID 6294BE9B
gpg: Good signature from "Debian CD signing key <debian-cd@lists.debian.org>" [unknown]
gpg: WARNING: This key is not certified with a trusted signature!
gpg:          There is no indication that the signature belongs to the owner.
Primary key fingerprint: DF9B 9C49 EAA9 2984 3258  9D76 DA87 E80D 6294 BE9B
$
```

"Good signature from" is the output line we're looking for. The warning about the key not being certified with a trusted signature is OK in this circumstance. The package the key came from was signed and apt verifies the package signature before installing, so the key was verified even if you don't have a web of trust connection to it.

To test them all, use the following loop. Adjust the value of **image** as necessary.

```shell
( image=debian-8.5.0-openstack; for sum in MD5SUMS SHA1SUMS SHA256SUMS SHA512SUMS; do LANG=C gpg --keyring /usr/share/keyrings/debian-role-keys.gpg --verify $image.$sum.sign; done; )
```

If running something other than debian, then see [debian's keyring page](https://keyring.debian.org/) for how to get the keys.

## Verifying checksums

To verify the checksums once you've verified the checksum files, run the appropriate checksum command against whichever checksum file you want to verify ( see the [Checksum sidebar](#Checksum_sidebar) for more information about checksums ).

The checksum files have checksums for more images than I downloaded, so this command grabs the image I'm interested in and verifies its checksum.

```
$ grep debian-8.5.0-openstack-amd64.qcow2$ debian-8.5.0-openstack.SHA512SUMS | sha512sum -c
debian-8.5.0-openstack-amd64.qcow2: OK
$ 
```

The "OK" is what informs us that the checksum matched.

```
$ ( for cksum in sha512sum sha256sum sha1sum md5sum; do echo -n "$cksum "; ext=$( echo $cksum | tr 'a-z' 'A-Z' )S; grep debian-8.5.0-openstack-amd64.qcow2$ debian-8.5.0-openstack.$ext | $cksum -c; done )
sha512sum debian-8.5.0-openstack-amd64.qcow2: OK
sha256sum debian-8.5.0-openstack-amd64.qcow2: OK
sha1sum debian-8.5.0-openstack-amd64.qcow2: OK
md5sum debian-8.5.0-openstack-amd64.qcow2: OK
$ 
```

## Sidebars

### <a name="GnuPG_sidebar">GnuPG sidebar</a>

GnuPG is the name of the project. gnupg and gnupg2 are the names of the packages in debian. gpg  and gpg2 are the main commands for the two releases.

Use the following commands to see if you have GnuPG installed:

```shell
type gpg gpg2
```

If one of them is installed, use what you have.

If neither of those can find a command, then install `gnupg` ( `gnupg2` is a dummy transitional package that installs `gnupg`' ).

```shell
sudo apt install gnupg
```

```
$ apt-cache show gnupg | grep ^Depends:
Depends: gpgv, libbz2-1.0, libc6 (>= 2.15), libreadline6 (>= 6.0), libusb-0.1-4 (>= 2:0.1.12), zlib1g (>= 1:1.1.4)
$
```

```
$ apt-cache show gnupg2 | grep ^Depends:
Depends: dpkg (>= 1.15.4) | install-info, gnupg-agent (= 2.0.26-6), libassuan0 (>= 2.0.1), libbz2-1.0, libc6 (>= 2.15), libcurl3-gnutls (>= 7.16.2), libgcrypt20 (>= 1.6.1), libgpg-error0 (>= 1.14), libksba8 (>= 1.2.0), libreadline6 (>= 6.0), zlib1g (>= 1:1.1.4)
$
```

Alternatively, gpgv can be used. It has less dependencies and smaller executable.

```
$ apt-cache show gpgv | grep ^Depends:
Depends: libbz2-1.0, libc6 (>= 2.14), zlib1g (>= 1:1.1.4)
$
```

If using gpgv, then it won't automagically determine which file to check.

```
$ gpgv --keyring /usr/share/keyrings/debian-role-keys.gpg debian-8.5.0-openstack.SHA512SUMS.sign debian-8.5.0-openstack.SHA512SUMS
gpgv: Signature made Thu Jun  9 19:39:03 2016 MST using RSA key ID 6294BE9B
gpgv: Good signature from "Debian CD signing key <debian-cd@lists.debian.org>"
$ 
```

### <a name="Checksum_sidebar">Checksum sidebar</a>

A checksum is a one way fingerprint on a file. A key feature is that checksums only work in one direction. You can consistently calculate the fingerprint from the file, but you can't use the fingerprint to figure out what file it came from.

Imagine looking at the shadow of something, except that you can't determine what is making the shadow from looking at the shadow.

Some checksums, such as md5, are no longer secure. It's possible to somewhat easily create new files that have the same md5 checksum as another file.

At this time it's better to use sha512sum.

It's also a good practice to check multiple checksums.

Using the shadow analogy, if you have multiple shadows ( first check for [Vashta Nerada](http://tardis.wikia.com/wiki/Vashta_Nerada) ), then it's more difficult to find a replacement object that will produce exactly the same shadows.

&copy; 2016, 2018 der.hans --- CC BY-SA 4.0 unported
